#!/bin/bash
git clone https://gitlab.inria.fr/starpu/starpu.git
cd starpu
./autogen.sh
mkdir build && cd build
../configure --enable-quick-check --disable-mpi-check
make
STARPU_MICROBENCHS_DISABLED=1 make -k check | tee  ../check_$$

make showcheckfailed

grep "^FAIL:" ../check_$$ || true
exit $(grep -c "^FAIL:" ../check_$$)
