/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2024-2024  University of Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */
#include <starpu.h>

extern unsigned nslicesx;
extern unsigned nslicesy;
extern unsigned xdim;
extern unsigned ydim;
extern unsigned zdim;

extern struct starpu_codelet mult_cl;

void parse_args(int argc, char **argv);

void init_problem_data(float **A_r, float **B_r, float **C_r);

void partition_mult_data(float *A, float *B, float *C, starpu_data_handle_t *A_handle_r, starpu_data_handle_t *B_handle_r, starpu_data_handle_t *C_handle_r);

int launch_tasks(starpu_data_handle_t A_handle, starpu_data_handle_t B_handle, starpu_data_handle_t C_handle);

void unpartition_mult_data(starpu_data_handle_t A_handle, starpu_data_handle_t B_handle, starpu_data_handle_t C_handle);
