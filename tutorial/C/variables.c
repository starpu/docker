/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2025-2025  University of Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#include <starpu.h>

void inc_cpu_func(void *descr[], void *_args)
{
	(void)_args;
	int *val = (int *)STARPU_VARIABLE_GET_PTR(descr[0]);
	*val += 1;
}

struct starpu_codelet inc_codelet =
{
	.modes = { STARPU_RW },
	.cpu_funcs = {inc_cpu_func},
	.cpu_funcs_name = {"inc_cpu_func"},
	.nbuffers = 1
};

void add_cpu_func(void *descr[], void *_args)
{
	(void)_args;
	int *a = (int *)STARPU_VARIABLE_GET_PTR(descr[0]);
	int *b = (int *)STARPU_VARIABLE_GET_PTR(descr[1]);
	*a += *b;
}

struct starpu_codelet add_codelet =
{
	.modes = { STARPU_RW, STARPU_R },
	.cpu_funcs = {add_cpu_func},
	.cpu_funcs_name = {"add_cpu_func"},
	.nbuffers = 2
};

void mult_cpu_func(void *descr[], void *_args)
{
	(void)_args;
	int *a = (int *)STARPU_VARIABLE_GET_PTR(descr[0]);
	int *b = (int *)STARPU_VARIABLE_GET_PTR(descr[1]);
	*a *= *b;
}

struct starpu_codelet mult_codelet =
{
	.modes = { STARPU_RW, STARPU_R },
	.cpu_funcs = {mult_cpu_func},
	.cpu_funcs_name = {"mult_cpu_func"},
	.nbuffers = 2,
	.name = "mult_codelet"
};

int main()
{
	int var1=0, var2=20, var3=0;
	starpu_data_handle_t handle1, handle2, handle3;
	int ret;

	ret = starpu_init(NULL);
	if (ret == -ENODEV) return 77;
	STARPU_CHECK_RETURN_VALUE(ret, "starpu_init");

	starpu_variable_data_register(&handle1, STARPU_MAIN_RAM, (uintptr_t)&var1, sizeof(var1));
	starpu_variable_data_register(&handle2, STARPU_MAIN_RAM, (uintptr_t)&var2, sizeof(var2));
	starpu_variable_data_register(&handle3, STARPU_MAIN_RAM, (uintptr_t)&var3, sizeof(var3));

	struct starpu_task *task1 = starpu_task_create();
	task1->name = "inc_var1";
	task1->cl = &inc_codelet;
	task1->handles[0] = handle1;
	starpu_task_submit(task1);

	struct starpu_task *task2 = starpu_task_create();
	task2->name = "addition";
	task2->cl = &add_codelet;
	task2->handles[0] = handle1;
	task2->handles[1] = handle2;
	starpu_task_submit(task2);

	struct starpu_task *task3 = starpu_task_create();
	task3->name = "inc_var2_1";
	task3->cl = &inc_codelet;
	task3->handles[0] = handle3;
	starpu_task_submit(task3);

	struct starpu_task *task4 = starpu_task_create();
	task4->name = "inc_var2_2";
	task4->cl = &inc_codelet;
	task4->handles[0] = handle3;
	starpu_task_submit(task4);

	struct starpu_task *task5 = starpu_task_create();
	task5->cl = &mult_codelet;
	task5->handles[0] = handle1;
	task5->handles[1] = handle3;
	starpu_task_submit(task5);

	starpu_task_wait_for_all();

	starpu_data_unregister(handle1);
	starpu_data_unregister(handle2);
	starpu_data_unregister(handle3);

	fprintf(stderr, "variable -> %d\n", var1);

	starpu_shutdown();

	return EXIT_SUCCESS;
}
