! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2022-2024   University of Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!
module vector_scal_cl
contains
        ! 'vector_scal' codelet routine
        !
        ! The "buffers" array matches the task->handles array: for instance
        ! task->handles[0] is a handle that corresponds to a data with
        ! vector "interface", so that the first entry of the array in the
        ! codelet  is a pointer to a structure describing such a vector (ie.
        ! struct starpu_vector_interface *). Here, we therefore manipulate
        ! the buffers[0] element as a vector: nx gives the number of elements
        ! in the array, ptr gives the location of the array (that was possibly
        ! migrated/replicated), and elemsize gives the size of each elements.
        !
recursive subroutine vector_scal_cpu(buffers, cl_args) bind(C)
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        implicit none

        type(c_ptr), value, intent(in) :: buffers, cl_args
        real, dimension(:), pointer :: val
        integer :: n_val,i
        real, target :: factor

        ! get 'val' number of elements
        n_val = fstarpu_vector_get_nx(buffers, 0)

        ! get 'val' converted Fortran pointer
        call c_f_pointer(fstarpu_vector_get_ptr(buffers, 0), val, shape=[n_val])

        ! get the scaling factor as inline argument
        call fstarpu_unpack_arg(cl_args, (/ c_loc(factor) /))

        do i=1,n_val
                val(i) = val(i)*factor
                !write(*,*) i,val(i)
        end do
end subroutine vector_scal_cpu

end module vector_scal_cl

module vector_scal_cl_cuda
implicit none

interface
        ! void vector_scal_cuda(void *buffers[], void *_args);
        subroutine vector_scal_cuda_cl(buffers, cl_args) &
                        bind(C,name="vector_scal_cuda")
                use iso_c_binding, only: c_ptr
                type(c_ptr), value, intent(in) :: buffers, cl_args
        end subroutine vector_scal_cuda_cl
end interface

end module vector_scal_cl_cuda
