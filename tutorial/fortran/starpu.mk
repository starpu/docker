# StarPU --- Runtime system for heterogeneous multicore architectures.
#
# Copyright (C) 2022-2024  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
#
# StarPU is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or (at
# your option) any later version.
#
# StarPU is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License in COPYING.LGPL for more details.
#

STARPU_VERSION=1.4

FC = gfortran
CC = gfortran
NVCC = nvcc

CFLAGS = $(shell pkg-config --cflags starpu-$(STARPU_VERSION))
LDLIBS =  $(shell pkg-config --libs starpu-$(STARPU_VERSION))
NVCCFLAGS = $(shell pkg-config --cflags starpu-$(STARPU_VERSION)) -std=c++11
FCFLAGS = -J. -cpp

FSTARPU_MOD = $(shell pkg-config --cflags-only-I starpu-$(STARPU_VERSION)|sed -e 's/^\([^ ]*starpu\/$(STARPU_VERSION)\).*$$/\1/;s/^.* //;s/^-I//')/fstarpu_mod.f90

# Automatically enable CUDA / OpenCL
STARPU_CONFIG=$(shell pkg-config --variable=includedir starpu-$(STARPU_VERSION))/starpu/$(STARPU_VERSION)/starpu_config.h
ifneq ($(shell grep "USE_CUDA 1" $(STARPU_CONFIG)),)
USE_CUDA=1
endif

all: $(TARGETS) $(PROG)

$(PROG): %: %.o $(OBJS)
	$(FC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

%.o: %.cu
	$(NVCC) $(NVCCFLAGS) -c -o $@ $<
%.o: %.f90
	$(FC) $(FCFLAGS) -c -o $@ $<
fstarpu_mod.o: $(FSTARPU_MOD)
	$(FC) $(FCFLAGS) -c -o $@ $<


clean:
	rm -fv *.o *.mod $(PROG) starpu.log


