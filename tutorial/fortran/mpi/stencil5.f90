! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2022-2024   University of Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!

module stencil5_cl
contains

recursive subroutine cl_cpu_stencil5_func (buffers, cl_args) bind(C)
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        implicit none

        type(c_ptr), value, intent(in) :: buffers, cl_args ! cl_args is unused
        real, pointer :: xy, xm1y, xp1y, xym1, xyp1

        call c_f_pointer(fstarpu_variable_get_ptr(buffers, 0), xy)
        call c_f_pointer(fstarpu_variable_get_ptr(buffers, 1), xm1y)
        call c_f_pointer(fstarpu_variable_get_ptr(buffers, 2), xp1y)
        call c_f_pointer(fstarpu_variable_get_ptr(buffers, 3), xym1)
        call c_f_pointer(fstarpu_variable_get_ptr(buffers, 4), xyp1)

        xy = (xy + xm1y + xp1y + xym1 + xyp1) / 5

end subroutine cl_cpu_stencil5_func

function my_distrib(x, y, size) bind(C)
        use iso_c_binding       ! C interfacing module

        integer(c_int), intent(in) :: x, y, size
        integer(c_int) :: my_distrib
        real :: sqrt_size = 1.414214

        ! keep 3 digits after the decimal point
        sqrt_size = (anint(sqrt(REAL(size))*1000))/1000

        my_distrib = mod(INT(x / sqrt_size  + (y / sqrt_size) * sqrt_size), size)

end function

function my_distrib2(x, y, size) bind(C)
        use iso_c_binding       ! C interfacing module

        integer(c_int), intent(in) :: x, y, size
        integer(c_int) :: my_distrib2

        my_distrib2 = mod((my_distrib(x, y, size) + 1), size)

end function

subroutine parse_args(niter, display) bind(C)
        use iso_c_binding, only: c_int
        integer :: i, j
        integer :: n
        integer(kind=8) :: m
        character(len=64) :: arg
        character(len=16) :: para

        integer(kind=8), intent(inout) :: niter
        integer(c_int), intent(inout) :: display

        n=iargc() !number of parameters
        do i = 1, n
                call getarg(i,arg)
                read(arg,'(A16)') para
                if(para == "-iter") then
                        j=i+1
                        call getarg(j,arg)
                        read(arg,'(I16)') m
                        niter = m
                else if (para == "-display") then
                        display = 1
                end if
        end do

end subroutine parse_args

end module stencil5_cl

program stencil5
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        use fstarpu_mpi_mod     ! StarPU MPI interfacing module
        use stencil5_cl
        implicit none


        real, allocatable, target :: matrix(:,:)
        integer(c_int) :: NX = 5
        integer(c_int) :: NY = 5
        integer(c_long) :: niter = 10
        integer(c_int) :: display = 0
        real :: mean = 0
        integer(c_int) :: x,y
        real :: rand_v
        integer(c_int) :: ret
        integer(c_int), target :: world
        integer(c_int) :: rank
        integer(c_int) :: size
        type(c_ptr), allocatable :: matrix_handle(:,:)
        type(c_ptr) :: stencil5_codelet   ! a pointer for the codelet structure
        integer(c_int) :: mpi_rank
        integer(c_long) :: loop
        integer(c_int) :: value = 0
        integer(c_int64_t) :: tag

        call parse_args(niter, display)

        ret = fstarpu_init(C_NULL_PTR)
        ret = fstarpu_mpi_init(1)

        world = fstarpu_mpi_world_comm()
        rank = fstarpu_mpi_world_rank()
        size = fstarpu_mpi_world_size()

        write(*,*) "rank=", rank, "size=", size

        !allocation
        allocate(matrix(NX,NY))
        allocate(matrix_handle(NX,NY))

        call random_seed()
        do x=1, NX
        do y=1, NY
                call random_number(rand_v)
                matrix(x,y) = rand_v
                mean = mean + matrix(x,y)
        end do
        end do

        mean = mean/(NX*NY)

        if(display == 1) then
                write(*,*) "mean=", mean
                do x=1, NX
                        write(*,*) "[", rank, "]"
                        do y=1, NY
                                write(*,*) matrix(x, y)
                        end do
                end do
        end if

        ! allocate an empty codelet structure
        stencil5_codelet = fstarpu_codelet_allocate()

        ! set the codelet name
        call fstarpu_codelet_set_name(stencil5_codelet, C_CHAR_"stencil5_codelet"//C_NULL_CHAR)

        ! add a CPU implementation function to the codelet
        call fstarpu_codelet_add_cpu_func(stencil5_codelet, C_FUNLOC(cl_cpu_stencil5_func))

        ! add a Read-Write mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(stencil5_codelet, FSTARPU_RW)
        ! add a Read mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(stencil5_codelet, FSTARPU_R)
        ! add a Read mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(stencil5_codelet, FSTARPU_R)
        ! add a Read mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(stencil5_codelet, FSTARPU_R)
        ! add a Read mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(stencil5_codelet, FSTARPU_R)

        ! Initial distribution
        tag = 0
        do x=1, NX
        do y=1, NY
                mpi_rank = my_distrib(x-1, y-1, size)
                if(rank == mpi_rank) then
                        !write(*,*) rank, "owning data", x, y
                        call fstarpu_variable_data_register(matrix_handle(x,y), 0, &
                         c_loc(matrix(x,y)), c_sizeof(matrix(1,1)))
                else if (rank == my_distrib(x, y-1, size) .or. rank == my_distrib(x-2, y-1, size) &
                                 .or. rank == my_distrib(x-1, y, size) .or. rank == my_distrib(x-1, y-2, size)) then
                        !write(*,*) rank, "neighbour of data", x, y
                        call fstarpu_variable_data_register(matrix_handle(x,y), -1, &
                        C_NULL_PTR, c_sizeof(matrix(1,1)))
                else
                   ! matrix_handle(x,y) = C_NULL_PTR
                   ! the insert_task fails with a null pointer
                   call fstarpu_variable_data_register(matrix_handle(x,y), -1, &
                        C_NULL_PTR, c_sizeof(matrix(1,1)))
                end if

                if(c_associated(matrix_handle(x,y))) then
                        !call fstarpu_data_set_coordinates(matrix_handle(x,y), 2, (/ x, y /))
                        call fstarpu_mpi_data_register(matrix_handle(x,y), tag, mpi_rank)
                        tag = tag+1
                end if
        end do
        end do

        do loop = 1, niter
                !call fstarpu_iteration_push(loop)
                do x=2, NX-1
                do y=2, NY-1
                        call fstarpu_mpi_task_insert((/ c_loc(world), stencil5_codelet, &
                        FSTARPU_RW,  matrix_handle(x,y), &
                        FSTARPU_R,  matrix_handle(x-1,y), &
                        FSTARPU_R,  matrix_handle(x+1,y), &
                        FSTARPU_R,  matrix_handle(x,y-1), &
                        FSTARPU_R,  matrix_handle(x,y+1), &
                        C_NULL_PTR /))
                end do
                end do
                !call fstarpu_iteration_pop()
        end do

        print *, "Waiting..."

        ! wait for task completion
        call fstarpu_task_wait_for_all()

        ! Now migrate data to a new distribution

        ! First register newly needed data
        tag = 0
        do x=1, NX
        do y=1, NY
                mpi_rank = my_distrib2(x-1, y-1, size)
                if((.not. c_associated(matrix_handle(x,y))) .and. ((rank == mpi_rank) &
                        .or. rank == my_distrib2(x, y-1, size) .or. rank == my_distrib2(x-2, y-1, size)&
                        .or. rank == my_distrib2(x-1, y, size) .or. rank == my_distrib2(x-1, y-2, size))) then

                        call fstarpu_variable_data_register(matrix_handle(x,y), -1, &
                        C_NULL_PTR, c_sizeof(matrix(1,1)))
                        call fstarpu_mpi_data_register(matrix_handle(x,y), tag, mpi_rank)
                        tag = tag+1
                end if

                if((c_associated(matrix_handle(x,y))) .and. (mpi_rank /= fstarpu_mpi_data_get_rank(matrix_handle(x,y)))) then
                        ! Migrate the data
                        call fstarpu_mpi_data_migrate(world, matrix_handle(x,y), mpi_rank)
                end if
        end do
        end do

        ! Second computation with new distribution
        do loop = 1, niter
                !call fstarpu_iteration_push(niter + loop)
                do x=2, NX-1
                do y=2, NY-1
                        call fstarpu_mpi_task_insert((/ c_loc(world), stencil5_codelet, &
                        FSTARPU_RW,  matrix_handle(x,y), &
                        FSTARPU_R,  matrix_handle(x-1,y), &
                        FSTARPU_R,  matrix_handle(x+1,y), &
                        FSTARPU_R,  matrix_handle(x,y-1), &
                        FSTARPU_R,  matrix_handle(x,y+1), &
                        C_NULL_PTR /))
                end do
                end do
                !call fstarpu_iteration_pop()
        end do

        print *, "Waiting..."

        ! wait for task completion
        call fstarpu_task_wait_for_all()

        ! unregister handle
        do x=1, NX
        do y=1, NY
                if(c_associated(matrix_handle(x,y))) then
                        mpi_rank = my_distrib(x-1, y-1, size)
                        ! Get back data to original place where the user-provided buffer is
                        call fstarpu_mpi_data_migrate(world, matrix_handle(x,y), mpi_rank)
                        call fstarpu_data_unregister(matrix_handle(x, y))
                end if
        end do
        end do


        ! free codelet structure
        call fstarpu_codelet_free(stencil5_codelet)

        ! shut MPI down
        ret = fstarpu_mpi_shutdown()

        ! shut StarPU down
        call fstarpu_shutdown()

        if(display == 1) then
                write(*,*) "[", rank, "] mean=", mean
                do x=1, NX
                        write(*,*) "[", rank, "]"
                        do y=1, NY
                                write(*,*) matrix(x, y)
                        end do
                end do
        end if

        deallocate(matrix)
        deallocate(matrix_handle)


end program stencil5
