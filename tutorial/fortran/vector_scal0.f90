! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2022-2024   University of Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!
module vector_scal_mod
contains
	! This kernel takes a buffer and scales it by a constant factor
	subroutine vector_scal_cpu(val, n, factor)
	    implicit none

	    real, dimension(:), intent(inout):: val
	    integer, intent(in) :: n
	    real, intent(in) :: factor
	    integer :: i

	    do i=1, n
	            val(i) = val(i)*factor
	            !write(*,*) i, val(i)
	    end do

	end subroutine vector_scal_cpu
end module vector_scal_mod

program vector_scal0
  use vector_scal_mod
    implicit none

    real, dimension(:), allocatable :: vector
    integer :: i, NX = 2048
    real :: factor = 3.14

    allocate(vector(NX))
    vector = REAL(1)

    write(*,*) "BEFORE: First element was", vector(1)

    call vector_scal_cpu(vector, NX, factor)

    write(*,*) "AFTER: First element is", vector(1)

    deallocate(vector)

end program vector_scal0
