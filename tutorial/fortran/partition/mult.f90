! StarPU --- Runtime system for heterogeneous multicore architectures.
!
! Copyright (C) 2022-2024   University of Bordeaux, CNRS (LaBRI UMR 5800), Inria
!
! StarPU is free software; you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation; either version 2.1 of the License, or (at
! your option) any later version.
!
! StarPU is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!
! See the GNU Lesser General Public License in COPYING.LGPL for more details.
!
!That program should compute C = A * B
!
!   A of size (z,y)
!   B of size (x,z)
!   C of size (x,y)
!
!              |---------------|
!            z |       B       |
!              |---------------|
!       z              x
!     |----|   |---------------|
!     |    |   |               |
!     |    |   |               |
!     | A  | y |       C       |
!     |    |   |               |
!     |    |   |               |
!     |----|   |---------------|
!
! Note: we use FORTRAN ordering.
!
program partition
        use iso_c_binding       ! C interfacing module
        use fstarpu_mod         ! StarPU interfacing module
        use mult_cl
        use mult_task
        implicit none

        real, dimension(:,:), allocatable, target :: ma, mb, mc

        type(c_ptr) :: perfmodel_mult   ! a pointer for the perfmodel structure
        type(c_ptr) :: cl_mult   ! a pointer for the codelet structure
        type(c_ptr) :: ma_handle, mb_handle, mc_handle   ! pointer for the matrix data handle
        integer(c_int) :: err   ! return status for fstarpu_init
        type(c_ptr) :: filter_horiz
        type(c_ptr) :: filter_vert

        integer(c_int) :: X = 1024
        integer(c_int) :: Y = 1024
        integer(c_int) :: Z = 512
        integer(c_int) :: Y_parts = 4
        integer(c_int) :: X_parts = 4

        call parse_args(X, Y, Z, X_parts, Y_parts)

        call init_problem_data(ma, mb, mc, X, Y, Z)

        ! initialize StarPU
        err = fstarpu_init(C_NULL_PTR)

        ! allocate an empty perfmodel structure
        perfmodel_mult = fstarpu_perfmodel_allocate()

        ! set the perfmodel symbol
        call fstarpu_perfmodel_set_symbol(perfmodel_mult, C_CHAR_"mult_perf"//C_NULL_CHAR)

        ! set the perfmodel type
        call fstarpu_perfmodel_set_type(perfmodel_mult, FSTARPU_HISTORY_BASED)

        ! allocate an empty codelet structure
        cl_mult = fstarpu_codelet_allocate()

        ! set the codelet perfmodel
        call fstarpu_codelet_set_model(cl_mult, perfmodel_mult)

        ! set the codelet name
        call fstarpu_codelet_set_name(cl_mult, C_CHAR_"mult_codelet"//C_NULL_CHAR)

        ! add a CPU implementation function to the codelet
        call fstarpu_codelet_add_cpu_func(cl_mult, C_FUNLOC(cl_cpu_mult_func))

        ! add Read mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(cl_mult, FSTARPU_R)
        ! add Read mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(cl_mult, FSTARPU_R)
        ! add Write mode data buffer to the codelet
        call fstarpu_codelet_add_buffer(cl_mult, FSTARPU_W)

        ! partition matrices into blocks that can be manipulated by the codelets
        call partition_mult_data(ma, mb, mc, ma_handle, mb_handle, mc_handle, X, Y, Z, X_parts, Y_parts)

        ! submit all tasks
        call launch_tasks(cl_mult, ma_handle, mb_handle, mc_handle, X_parts, Y_parts, X, Y, Z)

        ! wait for task completion
        call fstarpu_task_wait_for_all()

        ! unpartition
        call fstarpu_data_unpartition(ma_handle, 0)
        call fstarpu_data_unpartition(mb_handle, 0)
        call fstarpu_data_unpartition(mc_handle, 0)

        ! unregister
        call fstarpu_data_unregister(ma_handle)
        call fstarpu_data_unregister(mb_handle)
        call fstarpu_data_unregister(mc_handle)

        ! free codelet structure
        call fstarpu_codelet_free(cl_mult)

        ! shut StarPU down
        call fstarpu_shutdown()

        ! free perfmodel structure (must be called after fstarpu_shutdown)
        call fstarpu_perfmodel_free(perfmodel_mult)

        ! deallocation matrices
        deallocate(ma)
        deallocate(mb)
        deallocate(mc)

end program partition
